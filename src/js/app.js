import sayHello from './lib/sayHello';

sayHello();

// ie polifil START
(function(ELEMENT) {
	ELEMENT.matches = ELEMENT.matches || ELEMENT.mozMatchesSelector || ELEMENT.msMatchesSelector || ELEMENT.oMatchesSelector || ELEMENT.webkitMatchesSelector;
	ELEMENT.closest = ELEMENT.closest || function closest(selector) {
		if (!this) return null;
		if (this.matches(selector)) return this;
		if (!this.parentElement) {return null}
		return this.parentElement.closest(selector)
	};
}(Element.prototype));
// ie polifil END

// input file START
document.querySelector('.header--input-file a').addEventListener('click', (e) => {
	e.preventDefault();
	document.querySelector('input[type=file]').click();
});
// input file END

// input effects START
const inputs = Array.prototype.slice.call(document.querySelectorAll('.input-effect input'));
inputs.forEach((item) => {
	item.addEventListener('focusout', function a () {
		if(this.value !== '') {
			this.classList.add("has-content");
		} else {
			this.classList.remove("has-content");
		}
	})
});
// input effects END

// edit info START
const editBtns = Array.prototype.slice.call(document.querySelectorAll('.main--btn-edit'));
editBtns.forEach((item) => {
	item.addEventListener('click', function a () {
		// to hide other blocks
		const hoverBlocks = Array.prototype.slice.call(document.querySelectorAll('.hover-block'));
		if(hoverBlocks) {
			hoverBlocks.forEach((items) => {
				items.classList.remove('active');
			})
		}
		const content = item.parentNode.querySelector('span').textContent;
		const input = item.querySelector('.effect-16');
		if(content) {
			input.setAttribute('value', content);
			input.classList.add('has-content');
			item.querySelector('.hover-block').classList.add('active');
		} else {
			item.querySelector('.hover-block').classList.add('active');
			input.classList.remove('has-content');
		}
	}, true)
});
/* eslint no-param-reassign: ["error", { "props": false }] */
const save = Array.prototype.slice.call(document.querySelectorAll('.btn--save'));
const cancel = Array.prototype.slice.call(document.querySelectorAll('.btn--cancel'));
save.forEach((item) => {
	item.addEventListener('click', function a () {
		const {value} = this.closest('.hover-block').querySelector('input');
		const data = this.closest('.main--info-item').dataset.item;
		const toChangeItems = Array.prototype.slice.call(document.querySelectorAll(`.${  data}`));
		toChangeItems.forEach((iii) => {
		 iii.querySelector('span').innerHTML = value;
		});
		this.closest('.hover-block').classList.remove('active');
	}, false)
});
cancel.forEach((item) => {
	item.addEventListener('click', function a () {
		this.closest('.hover-block').classList.remove('active');
	}, false)
});
// edit info END

// nav STAR
function changeActiveBlock (name) {
	const blocks = Array.prototype.slice.call(document.querySelectorAll('.blocks'));
	blocks.forEach((item) => {
		item.classList.remove('active');
	});
	document.getElementById(name).classList.add('active');
}
const nav = Array.prototype.slice.call(document.querySelectorAll('nav a'));
nav.forEach((item) => {
	item.addEventListener('click', function (e) {
		e.preventDefault();
		nav.forEach((a) => {
			a.classList.remove('active')
		});
		this.classList.add('active');
		switch (item.dataset.block) {
			case 'about':
				changeActiveBlock('about');
				break;
			case 'settings':
				changeActiveBlock('settings');
				break;
			case 'option':
				changeActiveBlock('option');
				break;
			default:
				break;
		}
	})
});
// nav END

// mobile edit START
const about = document.querySelector('#about');
const aboutEdit = document.querySelector('#aboutEdit');
const dataItems = Array.prototype.slice.call(document.querySelectorAll('.main--info-item'));
document.querySelector('#mobileEdit').addEventListener('click', (e) => {
	e.preventDefault();
	about.classList.remove('active');
	aboutEdit.classList.add('active');
	const dataMobile = Array.prototype.slice.call(document.querySelectorAll('.hover-block--mobile input'));
	dataItems.forEach((item)=>{
		dataMobile.forEach((input) => {
			if(item.dataset.item === input.dataset.mobile){
				input.value = item.querySelector('span').textContent;
				input.classList.add("has-content");
			} else if (item.dataset.item === 'name') {
				const fullName = item.querySelector('span').textContent.split(' ');
				if(fullName[0]) {
					const firstName = document.querySelector('.first');
					// eslint-disable-next-line prefer-destructuring
					firstName.value = fullName[0];
					firstName.classList.add("has-content");
				}
				if(fullName[1]) {
					const lastName = document.querySelector('.last');
					// eslint-disable-next-line prefer-destructuring
					lastName.value = fullName[1];
					lastName.classList.add("has-content");
				}
			}
		})
	})
});
document.querySelector('#aboutEdit .cancel').addEventListener('click', () => {
	about.classList.add('active');
	aboutEdit.classList.remove('active');
});
document.querySelector('#aboutEdit .save').addEventListener('click', () => {
	const dataMobile = Array.prototype.slice.call(document.querySelectorAll('.hover-block--mobile input'));
	let first;
	let last;
	dataItems.forEach((item)=>{
		dataMobile.forEach((input) => {
			if(item.dataset.item === input.dataset.mobile){
				item.querySelector('span').textContent = input.value;
				document.querySelector(`.${  item.dataset.item} span`).textContent = input.value;
			} else if (input.dataset.mobile === 'first')  {
				first = input.value;
			} else if (input.dataset.mobile === 'last') {
				last = input.value;
			}
		});
		const names = Array.prototype.slice.call(document.querySelectorAll('.name'));
		names.forEach((item1) => {
			item1.textContent = `${first.replace(/\s/g, '')  } ${  last.replace(/\s/g, '')}`;
		});
	});
	about.classList.add('active');
	aboutEdit.classList.remove('active');
});
// mobile edit END

document.querySelectorAll('.nav--social-reviews-follower')[1].addEventListener('mouseenter', (e) => {
	document.querySelector('.popup').classList.add('show');
});
document.querySelectorAll('.nav--social-reviews-follower')[1].addEventListener('mouseleave', (e) => {
	document.querySelector('.popup').classList.remove('show');
});
